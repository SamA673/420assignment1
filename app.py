from flask import Flask, request, render_template
from math import sqrt
import random

app = Flask(__name__)

@app.route("/")
def home():
    return render_template('exercises.html')

@app.route("/exercise1")
def exercise1():
    return render_template('exercise1.html')

@app.route("/exercise2")
def exercise2():
    return render_template('exercise2.html')

@app.route("/exercise3")
def exercise3():
    return render_template('exercise3.html')

@app.route("/exercise4")
def exercise4():
    return render_template('exercise4.html')

@app.route("/exercise5")
def exercise5():
    return render_template('exercise5.html')












@app.route('/exercise1', methods=['POST'])
def exercise1_post():
    dividend = int(request.form['dividend'])
    divisor = int(request.form['divisor'])
    ans = dividend/divisor

    message = f'<ol> <li> The answer is {ans} </li>'

    if dividend % 2 == 0:
        message += '<li>' + str(dividend) + ' is even.' + '</li>'
    else:
        message += '<li>' + str(dividend) + ' is odd.' + '</li>'

    if dividend % 4 == 0:
        message += '<li>' +  str(dividend) + ' is multiple of 4.' + '</li>'

    if dividend % divisor == 0:
        message += '<li>' + str(dividend) + ' is a multiple of ' + str(divisor)+'.' + '</li>'

    message += ' </ol>'
    
    return message


@app.route('/exercise2', methods=['POST'])
def exercise2_post():
    name = request.form['name']
    score = int(request.form['grade'])

    # HashMap with grade range as key and grade value 
    grade_map = {
        (95, 100): 'A',
        (80, 94): 'B',
        (70, 79): 'C',
        (60, 69): 'D',
        (0, 59): 'F'
    }

    if score < 0 or score > 100:
        return f"<h1> wrong score: {score} </h1>"

    # We check every range to see IF score is a number BETWEEN LOWER AND UPPER
    # If SO, return "Your score is {score} --> you get {grade} "
    for (lower, upper), grade in grade_map.items():
        if lower <= score <= upper:
            return f"<h1> {name}'s score is {score} --> they get {grade} </h1>"


@app.route('/exercise3', methods=['POST'])
def exercise3_post():
    value = int(request.form['value'])

    if value <= 0:
        return f'<h1> {value} is equal to or less than 0 so... no sqrt...!'
    
    root = sqrt(value)
    return f'<h1> the sqrt of {value} is {root} </h1>'

@app.route('/exercise4', methods=['POST'])
def exercise4_post():
    number = int(request.form['number'])
    tries = 0

    if not(100 >= number >= 1):
        return f"<h1> Please input from 1 to 100 </h1>"

    while True:
        random_number = random.randint(1, 100)
        if random_number == number:
            return f" <h1>  IT TOOK {tries} TRIES TO GENERATE {number}"
        else:
            tries += 1

@app.route('/exercise5', methods=['POST'])
def exercise5_post():
    limit = int(request.form['number'])

    if limit <= 0:
        return f" <h1> {limit}: error... limit is <=0  </h1>"
    

    # initial fibonacci list...
    fibo_list = [0,1]
    
    while True: 
        new_num = (fibo_list[len(fibo_list) - 1] + fibo_list[len(fibo_list) - 2])

        if new_num < limit:
            fibo_list.append(new_num)
        else:
            break

    message =f'{limit}:' 

    for num in fibo_list:
        message += ' ' + str(num) + ','

    return f'<h1> {message}! </h1>'